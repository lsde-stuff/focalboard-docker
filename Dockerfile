FROM ubuntu:bionic
RUN apt update && apt install -y wget && rm -rf /var/cache/archives/*
WORKDIR /tmp
USER nobody
RUN wget https://github.com/mattermost/focalboard/releases/latest/download/focalboard-server-linux-amd64.tar.gz -O - | tar xvzf -
WORKDIR /tmp/focalboard
USER root
RUN mkdir -p /run/secrets/focalboard && rm -f config.json && ln -s /run/secrets/focalboard/config.json config.json
USER nobody
ENTRYPOINT ./bin/focalboard-server -config config.json
